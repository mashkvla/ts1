package hw1;

public class Factorial{

    public static void main(String[] args) {
        System.out.println(factorial(3));
    }

    public static int factorial(int number){
        if(number == 0){
            return 1;
        }
        int factorialNumber = number * factorial(number - 1);
        return factorialNumber;
    }
}
