package hw2;

public class Calculator {
    public int add(int a, int b){
        return a + b;
    }

    public int subtract(int a, int b){
        return a - b;
    }

    public int multiply(int a, int b){
        return a * b;
    }

    public int divide(int a, int b) throws Exception {
        if(b == 0){
            return exceptionThrown();
        }
        return a / b;
    }

    public int exceptionThrown() throws Exception {
        throw new Exception("Can't divide by 0");
    }

}
