package hw1Test;

import hw1.Factorial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FactorialTest {
    @Test
    public void factorialTest(){
        Factorial fac = new Factorial();
        int number = 5;
        long expectedResult = 120;

        long result = fac.factorial(number);

        Assertions.assertEquals(expectedResult, result);
    }
}
