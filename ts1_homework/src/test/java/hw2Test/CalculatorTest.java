package hw2Test;

import hw2.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import hw2.Calculator;

public class CalculatorTest {
    static Calculator calculator;

    @BeforeAll
    public static void calculatorInit() {
        calculator = new Calculator();
    }

    @Test
    public void addReturns15(){
        //Action
        calculatorInit();
        int a = 10;
        int b = 5;
        int result = calculator.add(a,b);
        int expectedResult = 15;
        //Assertion
        Assertions.assertEquals(result,expectedResult);
    }

    @Test
    public void subtractReturns13(){
        //Action
        calculatorInit();
        int a = 30;
        int b = 17;
        int result = calculator.subtract(a,b);
        int expectedResult = 13;
        //Assertion
        Assertions.assertEquals(result,expectedResult);
    }

    @Test
    public void multiplyReturns120(){
        //Action
        calculatorInit();
        int a = 30;
        int b = 4;
        int result = calculator.multiply(a,b);
        int expectedResult = 120;
        //Assertion
        Assertions.assertEquals(result,expectedResult);
    }

    @Test
    public void divideReturns120() throws Exception {
        //Action
        calculatorInit();
        int a = 45;
        int b = 5;
        int result = calculator.divide(a,b);
        int expectedResult = 9;
        //Assertion
        Assertions.assertEquals(result,expectedResult);
    }

    @Test
    public void divisionExceptionThrownException(){
        //Action
        Exception divisionBy0 = Assertions.assertThrows(Exception.class, () -> calculator.divide(10, 0));
        String expectedMessage = "Can't divide by 0";
        String message = divisionBy0.getMessage();
        //Assertion
        Assertions.assertEquals(expectedMessage, message);
    }

    @Test
    public void ifExceptionThrown(){
        //Action
        Exception divisionBy0 = Assertions.assertThrows(Exception.class, () -> calculator.exceptionThrown());
        String expectedMessage = "Can't divide by 0";
        String message = divisionBy0.getMessage();
        //Assertion
        Assertions.assertEquals(expectedMessage, message);
    }
}
