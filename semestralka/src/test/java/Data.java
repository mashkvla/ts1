import org.openqa.selenium.By;

public class Data {

    public String getUrl = "https://wa.toad.cz/~mashkvla/project/InTouch.php";

    public String adminLoginUsername = "admin";
    public String adminLoginPassword = "adminadmin";

    public String registrationUsername = "test5";
    public String registrationEmail = "test5@gmail.com";
    public String registrationPassword = "TestTest12";
    public String registrationDate = "12.12.2001";

    public String invalidUsername = "te1s23t4";
    public String invalidPassword = "Te2t1est12";
    public String invalidEmail = "test4@gmail";
    public String invalidDate = "12.12.0001";

    public By loginButton = By.linkText("LogIn");
    public By inputUsername = By.className("js-input-name");
    public By inputPassword = By.className("js-input-password");
    public By confirmLoginButton = By.id("log-in-but");

}
