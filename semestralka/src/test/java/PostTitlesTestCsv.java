import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PostTitlesTestCsv extends Data {

    private WebDriver driver;

    @Test
    public void checkPostTitlesFromCSV() throws IOException {
        // Initialize the WebDriver
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        // Reading the CSV file
        String csvFile = "posts_pages.csv";
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        String line;
        driver.get("https://wa.toad.cz/~mashkvla/project/InTouch.php?page=0");

        while ((line = br.readLine()) != null) {
            // Splitting the line by comma
            String[] data = line.split(",");
            driver.get("https://wa.toad.cz/~mashkvla/project/InTouch.php?page="+data[1]);

            // Finding the element based on the provided information from the CSV file
            WebElement element = driver.findElement(By.xpath("//div[@class = 'main-block']//h1[contains(text(),'" + data[0] + "')]"));
            Assert.assertEquals(element.getText(), data[0]);
        }
        driver.quit();
        br.close();
    }
}

