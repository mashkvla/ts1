import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class LoginTest extends Data {

    WebDriver driver = new ChromeDriver();
    Data getData = new Data();

    private By loginButton = By.linkText("LogIn");
    private By inputUsername = By.className("js-input-name");
    private By inputPassword = By.className("js-input-password");
    private By confirmLoginButton = By.id("log-in-but");

    private By errorMessage = By.className("wrong");

    @Test
    public void loginTest() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Click on the login button
        driver.findElement(loginButton).click();

        // Fill in the login form with valid credentials
        driver.findElement(inputUsername).sendKeys(getData.registrationUsername);
        driver.findElement(inputPassword).sendKeys(getData.registrationPassword);

        // Submit the login form
        driver.findElement(confirmLoginButton).click();

        //Checking if user is logged in
        String actualUsername = driver.findElement(By.id("username")).getText();

        Assert.assertEquals(actualUsername, "Uživatelské jméno: " + registrationUsername);

        sleep(2000);
        driver.quit();
    }

    @Test
    public void invalidLoginTest() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Click on the login button
        driver.findElement(loginButton).click();

        // Fill in the login form with invalid credentials
        driver.findElement(inputUsername).sendKeys(getData.invalidUsername);
        driver.findElement(inputPassword).sendKeys(getData.invalidPassword);

        // Submit the login form
        driver.findElement(confirmLoginButton).click();

        // Verify the error message is displayed
        String errorMsg = driver.findElement(errorMessage).getText();
        Assert.assertNotEquals("", errorMsg);

        sleep(3000);
        driver.quit();
    }
}
