import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class PostCreationTest extends Data {

    private String userProfilInfo = "Uživatelské jméno: admin\n" +
            "Email: admin@admin.com\n" +
            "Datum narození: 2002-02-26";

    private String testMessage = "testTestTest";
    private String testPhotoLink = "https://www.respawnpoint.cz/wp-content/uploads/2019/04/sony-ps5-playstation-5-hint-teaser-holiday-theme.jpg";

    private final WebDriver driver = new ChromeDriver();
    private Data getData = new Data();


    @Test
    public void postCreationAsAdmin() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Perform login as admin
        driver.findElement(getData.loginButton).click();
        driver.findElement(getData.inputUsername).sendKeys(getData.adminLoginUsername);
        driver.findElement(getData.inputPassword).sendKeys(getData.adminLoginPassword);
        driver.findElement(getData.confirmLoginButton).click();

        // Verify user profile information
        String pageUserInfo = driver.findElement(By.className("informacion")).getText();
        Assert.assertEquals(pageUserInfo, userProfilInfo);

        // Navigate to "InTouch" and add a new post
        driver.findElement(By.linkText("InTouch")).click();
        driver.findElement(By.linkText("Add new post")).click();
        driver.findElement(By.name("tittle")).sendKeys(testMessage);
        driver.findElement(By.name("article")).sendKeys(testMessage);
        driver.findElement(By.name("topImg")).sendKeys(testPhotoLink);
        driver.findElement(By.id("news")).click();
        driver.findElement(By.className("btn")).click();

        // Go to the main page and verify if the new post is displayed
        driver.findElement(By.linkText("Go to main page")).click();
        driver.get("https://wa.toad.cz/~mashkvla/project/InTouch.php?page=4");
        sleep(2000);
        WebElement newPostTitle = driver.findElement(By.linkText(testMessage));
        Assert.assertEquals(newPostTitle.getText(), testMessage);

        driver.quit();
    }
}