import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NewsCategoryTest extends Data{
    WebDriver driver = new ChromeDriver();

    private String expectedUrl = "https://wa.toad.cz/~mashkvla/project/category.php?category=novinky";

    @Test
    public void newsTestByUrl(){
        driver.get(getUrl);
        driver.findElement(By.linkText("Novinky")).click();
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals(currentUrl, expectedUrl);
    }
}
