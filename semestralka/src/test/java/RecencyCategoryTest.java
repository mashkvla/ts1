import org.junit.Assert;
        import org.junit.Test;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class RecencyCategoryTest extends Data{
    WebDriver driver = new ChromeDriver();

    private String expectedCategory = "Recenze";

    @Test
    public void recenciesTestByCategoryLabel(){
        driver.get(getUrl);
        driver.findElement(By.linkText("Recenze")).click();
        //Finding all posts on the page
        List<WebElement> postElements = driver.findElements(By.className("main-block"));
        for(WebElement webElement : postElements)
            checkCategory(webElement);
    }

    private void checkCategory(WebElement webElement) {
        WebElement categoryLabel = webElement.findElement(By.className("category"));
        String category = categoryLabel.getText();
        Assert.assertEquals(category, expectedCategory);
    }
}
