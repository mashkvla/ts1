import org.junit.Assert;
        import org.junit.Test;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.chrome.ChromeDriver;

        import java.io.BufferedReader;
        import java.io.FileReader;
        import java.io.IOException;

public class PostContainsInfoTestCsv extends Data {

    private WebDriver driver;

    @Test
    public void checkPostTitlesFromCSV() throws IOException {
        // Initialize the WebDriver
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        // Reading the CSV file
        String csvFile = "post_info.csv";
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        String line;
        driver.get(getUrl);

        while ((line = br.readLine()) != null) {
            // Splitting the line by comma
            String[] data = line.split(";");

            // Finding the element based on the provided information from the CSV file
            driver.findElement(By.xpath("//div[@class = 'main-block']//h1[contains(text(),'" + data[0] + "')]")).click();
            WebElement info = driver.findElement(By.id("post-text"));

            Assert.assertEquals(info.getText(), data[1]);
            driver.get(getUrl);
        }
        driver.quit();
        br.close();
    }
}


