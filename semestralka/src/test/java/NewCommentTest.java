import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class NewCommentTest extends Data {

    private String userProfilInfo = "Uživatelské jméno: admin\n" +
            "Email: admin@admin.com\n" +
            "Datum narození: 2002-02-26";

    private String testMessage = "testTestTest";

    private final WebDriver driver = new ChromeDriver();
    private Data getData = new Data();

    @Test
    public void newComment() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Perform login
        driver.findElement(getData.loginButton).click();
        driver.findElement(getData.inputUsername).sendKeys(getData.adminLoginUsername);
        driver.findElement(getData.inputPassword).sendKeys(getData.adminLoginPassword);
        driver.findElement(getData.confirmLoginButton).click();

        // Verify user profile information
        String pageUserInfo = driver.findElement(By.className("informacion")).getText();
        Assert.assertEquals(pageUserInfo, userProfilInfo);

        // Navigate to the desired page and add a new comment
        driver.findElement(By.linkText("InTouch")).click();
        driver.findElement(By.linkText("Ubisoft zrušil čtyři připravované hry")).click();
        driver.findElement(By.className("comment-area")).sendKeys(testMessage);
        driver.findElement(By.name("goComment")).click();

        // Verify that the new comment is added successfully
        WebElement newComment = driver.findElement(By.xpath("//div[@class = 'solo-user']//p[contains(text(),'" + testMessage + "')]"));
        Assert.assertEquals(newComment.getText(), testMessage);

        sleep(2000); // Wait for 2 seconds before quitting the driver
        driver.quit();
    }
}