import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class PostDeletingTest extends Data {

    private String userProfilInfo = "Uživatelské jméno: admin\n" +
            "Email: admin@admin.com\n" +
            "Datum narození: 2002-02-26";

    private final WebDriver driver = new ChromeDriver();
    private Data getData = new Data();

    @Test
    public void postDeletingAsAdmin() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Perform login as admin
        driver.findElement(getData.loginButton).click();
        driver.findElement(getData.inputUsername).sendKeys(getData.adminLoginUsername);
        driver.findElement(getData.inputPassword).sendKeys(getData.adminLoginPassword);
        driver.findElement(getData.confirmLoginButton).click();

        // Verify user profile information
        String pageUserInfo = driver.findElement(By.className("informacion")).getText();
        Assert.assertEquals(pageUserInfo, userProfilInfo);

        // Navigate to "InTouch" and delete a post
        driver.findElement(By.linkText("InTouch")).click();
        driver.findElement(By.linkText("Delete post")).click();
        driver.findElement(By.xpath("//table//tr[last()]//a[text()='Delete']")).click();

        // Go to the main page and verify if the post is deleted
        driver.findElement(By.linkText("Go to main page")).click();
        driver.get("https://wa.toad.cz/~mashkvla/project/InTouch.php?page=4");
        sleep(2000);

        driver.quit();
    }
}
