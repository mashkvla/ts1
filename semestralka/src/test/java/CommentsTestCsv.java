import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CommentsTestCsv extends Data{

    WebDriver driver = new ChromeDriver();
    Data getData = new Data();

    private String userProfilInfo = "Uživatelské jméno: admin\n" +
            "Email: admin@admin.com\n" +
            "Datum narození: 2002-02-26";

    @Test
    public void checkComments() throws IOException {

        driver.get(getData.getUrl);
        driver.manage().window().maximize();
        driver.findElement(getData.loginButton).click();
        driver.findElement(getData.inputUsername).sendKeys(getData.adminLoginUsername);
        driver.findElement(getData.inputPassword).sendKeys(getData.adminLoginPassword);
        driver.findElement(getData.confirmLoginButton).click();
        String pageUserInfo = driver.findElement(By.className("informacion")).getText();
        Assert.assertEquals(pageUserInfo, userProfilInfo);
        driver.findElement(By.linkText("InTouch")).click();

        // Reading the CSV file
        String csvFile = "comments.csv";
        BufferedReader br = new BufferedReader(new FileReader(csvFile));
        String line;
        while ((line = br.readLine()) != null) {
            // Splitting the line by comma
            String[] data = line.split(",");

            // Navigating to the website
            driver.findElement(By.linkText(data[0])).click();

            // Finding the element based on the provided information from the CSV file
            WebElement element = driver.findElement(By.xpath("//div[@class = 'solo-user']//p[contains(text(),'" + data[1] + "')]"));

            // Performing the desired checks/assertions
            if (element.getText().equals(data[2])) {
                System.out.println("PASS: Expected text matches actual text");
                driver.get(getData.getUrl);
            } else {
                System.out.println("FAIL: Expected text does not match actual text");
            }
        }
        driver.quit();
        br.close();
    }
}
