import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;

public class RegistrationTest extends Data {

    private By loginButton = By.linkText("LogIn");
    private By registrationButton = By.id("register-but");
    private By inputUsername = By.className("js-input-name");
    private By inputEmail = By.className("js-input-email");
    private By inputPassword = By.className("js-input-password");
    private By inputPasswordConformation = By.className("js-input-password-conf");
    private By inputCheckbox = By.id("checkbox");
    private By inputDate = By.id("date");

    private By errorMessage = By.className("wrong");

    private final WebDriver driver = new ChromeDriver();
    private Data getData = new Data();

    @Test
    public void userRegistration() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Click on the login button and navigate to registration page
        driver.findElement(loginButton).click();
        driver.findElement(registrationButton).click();

        // Fill in the registration form with valid data
        driver.findElement(inputUsername).sendKeys(getData.registrationUsername);
        driver.findElement(inputEmail).sendKeys(getData.registrationEmail);
        driver.findElement(inputPassword).sendKeys(getData.registrationPassword);
        driver.findElement(inputPasswordConformation).sendKeys(getData.registrationPassword);
        driver.findElement(inputDate).sendKeys(getData.registrationDate);
        driver.findElement(inputCheckbox).click();

        // Submit the registration form
        driver.findElement(registrationButton).click();

        sleep(3000);
        driver.quit();
    }

    @Test
    public void invalidUserRegistration() throws InterruptedException {
        // Open the website URL
        driver.get(getData.getUrl);
        driver.manage().window().maximize();

        // Click on the login button and navigate to registration page
        driver.findElement(loginButton).click();
        driver.findElement(registrationButton).click();

        // Fill in the registration form with invalid data
        driver.findElement(inputUsername).sendKeys(getData.invalidUsername);
        driver.findElement(inputEmail).sendKeys(getData.invalidEmail);
        driver.findElement(inputPassword).sendKeys(getData.invalidPassword);
        driver.findElement(inputPasswordConformation).sendKeys(getData.invalidPassword);
        driver.findElement(inputDate).sendKeys(getData.invalidDate);
        driver.findElement(inputCheckbox).click();

        // Submit the registration form
        driver.findElement(registrationButton).click();

        // Verify the error message is displayed
        String errorMsg = driver.findElement(errorMessage).getText();
        Assert.assertNotEquals("", errorMsg);

        sleep(3000);
        driver.quit();
    }
}
